# Examen

Liste de étapes pour mener à bien le projet kubernetes

## Kubeconfig

Récupérer le fichier kubeconfig.yml

```SH
export KUBECONFIG=<absolute-path-to>/kubeconfig.yml
```

Vérifier que le fichier kubeconfig est bien configuré

```SH
kubectl config current-context

kubernetes-admin@ICVAD_eval
```

Changer le contexte de votre kubeconfig pour utiliser le namespace

```SH
kubectl config set-context --current --namespace=ddjordjevic
```

## Cluster RabbitMQ

Créer le cluster rabbitMQ

```SH
kubectl apply -f rabbitmq-cluster.yaml
kubectl get pods

NAME                                         READY   STATUS    RESTARTS   AGE
production-rabbitmqcluster-server-0          1/1     Running   0          58m
production-rabbitmqcluster-server-1          1/1     Running   0          58m
production-rabbitmqcluster-server-2          1/1     Running   0          58m
```

## Database PostgreSQL

### Image Docker de la BDD sur mon repo

Créer l'image docker et la pousser sur mon hub

```SH
docker login
docker build -t mando99/counter-database .
docker push mando99/counter-database
```

### Deploiement de la BDD

Création du fichier de deploiement (+ service)

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: counter-database-deployment
  namespace: ddjordjevic
spec:
  replicas: 1
  selector:
    matchLabels:
      app: counter-database
  template:
    metadata:
      labels:
        app: counter-database
    spec:
      containers:
        - name: counter-database
          image: mando99/counter-database:latest
          ports:
            - containerPort: 5432

---

apiVersion: v1
kind: Service
metadata:
  name: counter-database-service
  namespace: ddjordjevic
spec:
  selector:
    app: counter-database
  ports:
    - protocol: TCP
      port: 5432
      targetPort: 5432
  type: LoadBalancer
```

Il faut lancer le fichier de deploiement

```shell
kubectl apply -f deployment.yaml
kubectl get pods

NAME                                         READY   STATUS    RESTARTS   AGE
counter-database-deployment-b785554f-rjr7s   1/1     Running   0          135m
production-rabbitmqcluster-server-0          1/1     Running   0          58m
production-rabbitmqcluster-server-1          1/1     Running   0          58m
production-rabbitmqcluster-server-2          1/1     Running   0          58m
```

Puis vérifier les services

```shell
kubectl get services  
                                 
NAME                               TYPE           CLUSTER-IP    EXTERNAL-IP      PORT(S)                                          AGE
counter-database-service           LoadBalancer   10.3.63.128   162.19.110.153   5432:32685/TCP                                   3h7m
production-rabbitmqcluster         LoadBalancer   10.3.248.68   162.19.109.191   5672:30296/TCP,15672:30809/TCP,15692:31105/TCP   105m
production-rabbitmqcluster-nodes   ClusterIP      None          <none>           4369/TCP,25672/TCP                               105m
```

## Deploiement du serveur

### Dockerfile.main

Dans un premier temps, il faut créer le Dockerfile

```Dockerfile
FROM node:18-alpine
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
EXPOSE 4040
CMD yarn migrate && yarn start
```

Créer l'image

```shell
docker build -t mando99/counter-server -f ./Dockerfile.main .
docker push mando99/counter-server
```

Attention à appliquer la migration à l'execution du conteneur, et non à la construction de l'image.

### Deploiement + Service

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: counter-server-deployment
  namespace: ddjordjevic
spec:
  replicas: 1
  selector:
    matchLabels:
      app: counter-server
  template:
    metadata:
      labels:
        app: counter-server
    spec:
      containers:
        - name: counter-server
          image: mando99/counter-server:latest
          env:
            - name: RABBITMQ_URL
              value: "amqp://production-rabbitmqcluster:5672"
            - name: PGHOST
              value: "counter-database-service"
            - name: PGPORT
              value: "5432"
          ports:
            - containerPort: 4040

---

apiVersion: v1
kind: Service
metadata:
  name: counter-server-service
  namespace: ddjordjevic
spec:
  selector:
    app: counter-server
  ports:
    - protocol: TCP
      port: 4040
      targetPort: 4040
  type: LoadBalancer
```

Ne pas oublier d'ajouter les variables d'environnements nécessaires, l'url de la BDD correspond au nom du service, de même pour le cluster rabbitMQ

Lancer le deploiement

```shell
kubectl apply -f deployment.yaml
kubectl get pods

NAME                                         READY   STATUS    RESTARTS   AGE
counter-database-deployment-b785554f-rjr7s   1/1     Running   0          140m
counter-server-deployment-7588bc8869-hwlwt   1/1     Running   0          51m
production-rabbitmqcluster-server-0          1/1     Running   0          58m
production-rabbitmqcluster-server-1          1/1     Running   0          58m
production-rabbitmqcluster-server-2          1/1     Running   0          58m
```

```shell
kubectl get services

NAME                               TYPE           CLUSTER-IP     EXTERNAL-IP      PORT(S)                                          AGE
counter-database-service           LoadBalancer   10.3.63.128    162.19.110.153   5432:32685/TCP                                   3h34m
counter-server-service             LoadBalancer   10.3.201.149   162.19.108.109   4040:32174/TCP                                   2m8s
production-rabbitmqcluster         LoadBalancer   10.3.248.68    162.19.109.191   5672:30296/TCP,15672:30809/TCP,15692:31105/TCP   133m
production-rabbitmqcluster-nodes   ClusterIP      None           <none>           4369/TCP,25672/TCP                               133m

```

Il faut vérifier que l'execution du pod s'est bien déroulé

```shell
kubectl logs counter-server-deployment-7588bc8869-hwlwt

yarn run v1.22.19
$ caravel migrate
🎆  Connected to DB.
👌 Migration table exists
📈 Getting all migrations in table...
📄 Getting all migrations from folder...
🙌 Database is up to date!
🤝 Migrations finished successfully !
Done in 0.26s.
yarn run v1.22.19
$ ts-node src/main
[2024-05-24T10:49:50.558Z](at Object):  {
  user: 'count',
  host: 'counter-database-service',
  database: 'count',
  port: '5432',
  password: 'count'
}
[2024-05-24T10:49:50.724Z](at listenRabbit):  trying to connect to  amqp://production-rabbitmqcluster:5672
[2024-05-24T10:49:50.728Z](at origin):  undefined
--> Routes:  {
  GET: [ '/', '/routes', '/count', '/count/:id' ],
  POST: [ '/count/:id/add', '/count/:id/reset', '/count/create' ],
  PATCH: [ '/count/delete' ],
  NOT_FOUND: [ '/' ]
}
Postgres: Connection success
[2024-05-24T10:49:50.793Z](at listenRabbit):  connected to  amqp://production-rabbitmqcluster:5672
```
La migration a réussi ainsi que la connexion avec RabbitMQ

Si on tente l'url sur un navigateur avec l'IP externe ```162.19.108.109:4040```, un message s'affiche ``ok``

Ajouter l'auto scaling et l'executer

```yaml
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: rabbitmq-autoscaling
  namespace: ddjordjevic
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: counter-server
  minReplicas: 1
  maxReplicas: 10
  targetCPUUtilizationPercentage: 50
```

Ainsi 
```shell
curl 162.19.108.109:4040/count/create -X POST

{"id":"2f71400a-716b-4238-9ccc-10322d4ece41","value":"0","created_at":"2024-05-24T12:59:09.493Z","updated_at":"2024-05-24T12:59:09.493Z"}%    
```

## Test application

### Dockerfile.count

```Dockerfile
FROM node:18-alpine
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
CMD ["node", "count.js"]
```

Créer l'image

```SH
docker build -t mando99/counter -f ./Dockerfile.counter .
docker push mando99/counter
```

### count-deployment.yaml

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: counter-deployment
  namespace: ddjordjevic
spec:
  replicas: 1
  selector:
    matchLabels:
      app: counter
  template:
    metadata:
      labels:
        app: counter
    spec:
      containers:
        - name: counter
          image: mando99/counter:latest
          env:
            - name: RABBITMQ_URL
              value: "amqp://production-rabbitmqcluster:5672"
            - name: INTERVAL
              value: "10"
          ports:
            - containerPort: 8080

---

apiVersion: v1
kind: Service
metadata:
  name: counter-service
  namespace: ddjordjevic
spec:
  selector:
    app: counter
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 8080
  type: LoadBalancer
```
Resultat

```shell
kubectl get pods
NAME                                         READY   STATUS        RESTARTS   AGE
counter-database-deployment-b785554f-rjr7s   1/1     Running       0          4h44m
counter-deployment-6d867cb4bc-xmq6g          1/1     Running       0          9s
counter-server-deployment-7588bc8869-vt6wl   1/1     Running       0          71m
production-rabbitmqcluster-server-0          1/1     Running       0          3h23m
production-rabbitmqcluster-server-1          1/1     Running       0          3h23m
production-rabbitmqcluster-server-2          1/1     Running       0          3h23m

drbook@MacBook_Pro_De_Darko backend % kubectl logs counter-deployment-6d867cb4bc-xmq6g
trying to connect to  amqp://production-rabbitmqcluster:5672
send
send
send
send
send
send
send
send
send
...
```

# Fin
